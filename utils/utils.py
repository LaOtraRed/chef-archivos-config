#!/usr/bin/env python3
import os
import sys
import re

def ipv4Valido(ipv4):
    ipv4regex = '^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$'
    regex = re.compile(ipv4regex)
    return regex.match(ipv4) is not None

def ipv6Valido(ipv6):
    ipv6regex = '(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))'
    regex = re.compile(ipv6regex)
    return regex.match(ipv6) is not None

## utils
def contenidoArchivo(filename=None):
    ''' Retorna una cadena con el contenido del archivo dado'''
    if filename is None:
        return None
    c = ''
    try:
        with open(filename, 'r') as fil:
            c = fil.read()
    except IOError as e:
        print (e)
        return None
    return c        

def escribirArchivo(filename=None, contenido=None, append=False):
    ''' Escribe el `contenido' en el archivo `filename'
    devuelve False si hay errores'''
    mode = 'w'
    if append:
        mode = 'a'
    if filename is None:
        return False
    try:
        with open(filename, mode) as fil:
            fil.write(contenido)
    except IOError as e:
        print (e)
        return False
    return True

def reemplazarArchivo(archivoFuente, archivoDestino, dictParametros):
    '''
    Reemplaza el contenido del `archivoFuente' con los datos que encuentra en `dictParametros' buscando las variables que coinciden
    Esta funcion reemplaza el contenido del archivo con `nombreArchivo' usando los datos de los parametros en el `archivoDestino'

    archivoFuente: Nombre del archivo Fuente
    dictParametros: Diccionario con los parametros para ser reemplazados
    archivoDestino: Nombre del archivo donde se copia el contenido reemplazado    
    '''
    content = contenidoArchivo(archivoFuente)
    for key, value in dictParametros.items():
        content = content.replace("{{" + key + "}}", str(value))
    return escribirArchivo(archivoDestino, content)

def agregarPrefijoAArchivo(archivoFuente, archivoDestino, prefijo, append=False):
    '''
    Agrega al principio de cada linea del `archivoFuente' el `prefijo' y lo guarda en el `achivoDestino'
    '''
    
    contenido = contenidoArchivo(archivoFuente)
    nuevoContenido = ''
    for linea in contenido.split(os.linesep):
        nuevoContenido += prefijo + linea + os.linesep
    return escribirArchivo(archivoDestino, nuevoContenido, append)
