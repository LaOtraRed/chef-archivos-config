# Generador de archivos de configuracion LOR

Genera archivos de configuración necesarios para tener un nodo funcionando en LaOtraRed usando el firmware openwrt.

Se utilizan la utilidad [uci][uci] de openwrt para modificar los archivos:

* network
* bmx7
* firewall
* dnsmasq
* dhcp
* luci

Este script genera otro script con comandos que `uci` interpreta para aplicar las configuraciones en el enrutador objetivo. El script generado debe copiarse y ejecutarse en el enrutador.

[uci]: https://openwrt.org/docs/guide-user/base-system/uci "Sistema uci"

LICENCIA: [GPLv3](LICENCSE)

## Instalación

Require: python3

clonar o descargar este proyecto

    git clone https://gitlab.com/LaOtraRed/chef-archivos-config
	cd chef-archivos-config

### Ejemplos de uso

```bash
python3 chef.py 10.64.3.66/27 fc01:1934:1:9::1/56
python3 chef.py 10.64.3.66/27 fc01:1934:1:2::1/56 --wifi_2G4_channel=9 --wifi_2G4_ap_ssid=mi_red_wifi
```

Se genera un archivo `resultados/uci.sh`, ese archivo debe copiarse en el enrutador y ejecutarlo.

```bash
# en el enrutador
chmod +x uci.sh
./uci.sh
```


	
