#!/usr/bin/env python3
'''
Ejemplos de uso:

python3 chef.py 10.64.3.66/27 fc01:1934:1:9::1/56
python3 chef.py 10.64.3.66/27 fc01:1934:1:2::1/56 --wifi_2G4_channel 9 wifi_2G4_ap_ssid mi_red_wifi
'''

import os
import sys
import argparse
import ipaddress
from utils.utils import ipv4Valido, ipv6Valido, contenidoArchivo, escribirArchivo, reemplazarArchivo, agregarPrefijoAArchivo

dict_network = {
    # ipv4
    'lan_ipv4_addr': '10.64.1.1',
    'lan_ipv4_netmask': '255.255.255.224',
    # ipv6
    'lan_ipv6_prefix': '01:1934:1:100::1/64',
    # wan
    'wan_ipaddr': '192.168.1.2',
    'wan_netmask': '255.255.255.0',
    'wan_gateway': '192.168.1.1',
    'wan_dns': '192.168.1.1',
}
dict_wireless = {
    'wifi_2G4_channel': '8',
    'wifi_2G4_ap_ssid': 'Nodo LOR',
    'wifi_5G_channel': '41',
    'wifi_5G_ssid': 'Nube LOR',
}
dict_bmx7 = {
    #         bmx7
    'bmx7_ipv6_uHna_prefix': 'fc01:1934:1:100::/56',
    'bmx7_ipv4_addr_and_prefix': '10.64.1.1/27',
    'bmx7_ipv6_addr_and_prefix': 'fc01:1934:1:100::1/128',
    'bmx7_ipv4_anycast_network_prefix': '10.64.64.0/24',
    'bmx7_ipv6_anycast_network_prefix': 'fc01:1934:a::/48',
    'bmx7_ipv4_advertisements_in_network_prefix': '10.64.0.0/15',
    'bmx7_ipv4_rfc1918_network_prefix': '10.0.0.0/8',
    'bmx7_ipv4_pit_lor_network_prefix': '0.0.0.0/0',
    'bmx7_ipv6_nube_lor_network_prefix': 'fd66:66:66::/48',
}
otras_etiquetas = {}

dirSalida = 'output'

def reemplazarVariables(args):
    '''
    Realiza el reemplazo de variables segun los argumentos introducidos
    retorna tres diccionarios con las variables reemplazadas
    dictNetwork, dictWireless y dictBmx7
    '''
    # dict_network
    dictNetwork = dict_network
    ipv4i = ipaddress.IPv4Interface(args.ipv4_prefix)
    ipv6i = ipaddress.IPv6Interface(args.ipv6_prefix)
    dictNetwork['lan_ipv4_addr'] = ipv4i.ip.compressed
    dictNetwork['lan_ipv6_addr'] = ipv6i.ip.compressed
    dictNetwork['lan_ipv4_netmask'] = ipv4i.netmask.compressed
    dictNetwork['lan_ipv6_prefix'] = ipv6i.compressed
    if vars(args).get('wan_ipaddr', None) is not None:
        dictNetwork['wan_ipaddr'] = vars(args)['wan_ipaddr']
    if vars(args).get('wan_netmask', None) is not None:
        dictNetwork['wan_netmask'] = vars(args)['wan_netmask']
    if vars(args).get('wan_gateway', None) is not None:
        dictNetwork['wan_gateway'] = vars(args)['wan_gateway']
    if vars(args).get('wan_dns', None) is not None:
        dictNetwork['wan_dns'] = args['wan_dns']
    # dict_wireless
    dictWireless = dict_wireless
    if vars(args).get('wifi_2G4_channel', None) is not None:
        dictNetwork['wifi_2G4_channel'] = vars(args)['wifi_2G4_channel']
    if vars(args).get('wifi_2G4_ap_ssid', None) is not None:
        dictNetwork['wifi_2G4_ap_ssid'] = vars(args)['wifi_2G4_ap_ssid']
    if vars(args).get('wifi_5G_channel', None) is not None:
        dictNetwork['wifi_5G_channel'] = vars(args)['wifi_5G_channel']
    if vars(args).get('wifi_5G_ap_ssid', None) is not None:
        dictNetwork['wifi_5G_ap_ssid'] = vars(args)['wifi_5G_ap_ssid']
    # dict_bmx7
    dictBmx7 = dict_bmx7
    dictBmx7['bmx7_ipv6_uHna_prefix'] = ipv6i.network.compressed
    dictBmx7['bmx7_ipv4_addr_and_prefix'] = ipv4i.network.compressed
    dictBmx7['bmx7_ipv6_addr_and_prefix'] = ipv6i.compressed

    return dictNetwork, dictWireless, dictBmx7
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Opciones de creación',)
    parser.add_argument("ipv4_prefix", type=str, help="Prefijo ipv4 principal ej: 10.64.2.0/27")
    parser.add_argument("ipv6_prefix", type=str, help="Prefijo ipv6 principal ej: fc01:1934:1:1::/56")
    parser.add_argument("--wan-ipv4", type=str, help="Dirección ipv4 wan del router o punto de acceso que reparte internet")
    parser.add_argument("--wan-dns", type=str, help="Dirección ipv4 del dns principal resolvedor hacia")
    parser.add_argument("--hostname", type=str, help="Nombre del host")
    parser.add_argument("--wifi_2G4_channel", type=str, help="Canal wifi para 2.4Ghz")
    parser.add_argument("--wifi_2G4_ap_ssid", type=str, help="SSID para el AP de la señal de 2.4Ghz")
    parser.add_argument("--wifi_5G_channel", type=str, help="Canal wifi para 5Ghz")
    parser.add_argument("--wifi_5G_ap_ssid", type=str, help="SSID para el AP de la señal de 5Ghz")
    
    args = parser.parse_args()

    try:
        ipaddress.IPv4Interface(args.ipv4_prefix)
    except Exception as e:
        print ('ipv4_prefix incorrecta', args.ipv4_prefix, '->', str(e))
        exit(1)

    try:
        ipaddress.IPv6Interface(args.ipv6_prefix)
    except Exception as e:
        print ('ipv6_prefix incorrecta', args.ipv6_prefix, '->', str(e))
        exit(1)


    dictNetwork, dictWireless, dictBmx7 = reemplazarVariables(args)
    for k,v in dictNetwork.items():
        print(k + ':' + v)
    for k,v in dictWireless.items():
        print(k + ':' + v)
    for k,v in dictBmx7.items():
        print(k + ':' + v)        
    # reemplazando archivo por archivo y guardando contenido
    print('---- reemplazos ----')
    print('uci/network', str(reemplazarArchivo('uci/network', 'resultados/network', dictNetwork)))
    print('uci/wireless', str(reemplazarArchivo('uci/wireless', 'resultados/wireless', dictWireless)))
    print('uci/bmx7', str(reemplazarArchivo('uci/bmx7', 'resultados/bmx7', dictBmx7)))
    # uniendo todas las configs en un solo archivo
    destFile = os.path.join(os.path.curdir + os.path.sep + 'resultados/uci.sh')
    if os.path.isfile(destFile):
        # ecribiendo primera linea
        escribirArchivo(destFile, '#!/bin/sh' + os.linesep)

    agregarPrefijoAArchivo('resultados/network', destFile, 'uci set ', append=True)
    escribirArchivo(destFile, 'uci commit network' + os.linesep, append=True)
    agregarPrefijoAArchivo('resultados/wireless', destFile, 'uci set ', append=True)
    escribirArchivo(destFile, 'uci commit wireless' + os.linesep, append=True)
    agregarPrefijoAArchivo('resultados/bmx7', destFile, 'uci set ', append=True)
    escribirArchivo(destFile, 'uci commit bmx7' + os.linesep, append=True)
    agregarPrefijoAArchivo('uci/firewall', destFile, 'uci set ', append=True)
    escribirArchivo(destFile, 'uci commit firewall' + os.linesep, append=True)
    agregarPrefijoAArchivo('uci/luci', destFile, 'uci set ', append=True)
    escribirArchivo(destFile, 'uci commit luci' + os.linesep, append=True)
    agregarPrefijoAArchivo('uci/dhcp', destFile, 'uci set ', append=True)
    escribirArchivo(destFile, 'uci commit dhcp' + os.linesep, append=True)
    # lineas que ejecutan las configs
    escribirArchivo(destFile, 'exit 0', append=True)

    print ('------- terminado (resultados/uci.sh) -------')
    print (contenidoArchivo(destFile))
        






        
